import React, { useState } from "react";
import gql from "graphql-tag";
import { useQuery } from "@apollo/react-hooks";
import { ListNamesQuery } from "./__generated__/ListNamesQuery";
import { FilteredJobsQuery } from "./__generated__/FilteredJobsQuery";
import styled from "styled-components";
import ReactTooltip from "react-tooltip";

// Grab names of all available lists, so user knows what their options are.
const LISTNAMES_QUERY = gql`
  query ListNamesQuery {
    lists {
      id
      name
      description
      image
    }
  }
`;

// Grab jobs for selected lists
const FILTEREDJOBS_QUERY = gql`
  query FilteredJobsQuery($lists: [ID]!) {
    filteredJobs(lists: $lists) {
      title
      company
      location
      salaryMin
      salaryMax
      applyLink
    }
  }
`;

// Render names of all available lists.
const RenderListOptions = (prop: {
  lists: (string | undefined)[];
  descriptions: Array<string | undefined>;
  updateSelectedLists: Function;
}) => {
  return (
    <div style={{ textAlign: "center", display: "inline-block" }}>
      <p>Hover over each list name below to learn more:</p>
      <h4 style={{ textAlign: "left", marginLeft: "80px" }}>
        {prop.lists?.map((name, i) => (
          <>
            <div key={i}>
              <input
                type="checkbox"
                value={name?.toString()}
                onChange={(e) =>
                  prop.updateSelectedLists(e.target.value, e.target.checked)
                }
              ></input>
              <span data-tip={prop.descriptions[i]}>{name}</span>
            </div>
            <ReactTooltip />
          </>
        ))}
      </h4>
    </div>
  );
};

// Interface to describe backend response (data models).
interface job {
  title: string;
  company: string;
  location: string;
  salary_min: number;
  salary_max: number;
  apply_link: string;
}

interface list {
  id: number;
  name: string;
  description: string;
  image: string;
}

// Query for and render filtered jobs.
const DisplayJobs = (prop: { listIDs: number[] }) => {
  const { loading, error, data } = useQuery<FilteredJobsQuery>(
    FILTEREDJOBS_QUERY,
    { variables: { lists: prop.listIDs } }
  );
  let filteredJobs = data?.filteredJobs;
  let displayJobs;
  displayJobs = filteredJobs?.map((job, i) => {
    return (
      <div key={i}>
        {i > 0 ? <hr></hr> : ""}
        {loading && <h1>Loading</h1>}
        {error && <h1>Error</h1>}
        Title:{" "}
        <a href={job?.applyLink || ""} target="_blank">
          {job?.title}
        </a>
        , <br></br>
        Company: {job?.company}, <br></br>
        Location: {job?.location},<br></br>
        Salary Range: ${job?.salaryMin.toLocaleString()} -{" "}
        {job?.salaryMax.toLocaleString()}
      </div>
    );
  });
  return (
    <>
      {prop.listIDs.length !== 0 ? (
        displayJobs
      ) : (
        <em>Select at least one job list from above to view jobs!</em>
      )}
    </>
  );
};

// Update what to render based on selected lists.
const ListJobs = () => {
  var [selectedLists, setSelectedLists] = useState<Array<string>>([]);
  var [selectedListIDs, setSelectedListIDs] = useState<Array<number>>([]);

  // Update selected lists when user checks or unchecks a list.
  const updateSelectedLists = (name: string, checked: Boolean) => {
    let tempArray: Array<string> = [...selectedLists];
    let tempArrayIDs: Array<number> = [...selectedListIDs];
    let id = allListNames.indexOf(name) + 1; // ID of the list we want to add.
    if (checked === true) {
      tempArray.push(name);
      tempArrayIDs.push(id);
    } else {
      let index = tempArray.indexOf(name);
      if (index > -1) {
        tempArray.splice(index, 1);
        tempArrayIDs.splice(index, 1);
      }
    }
    setSelectedLists(tempArray);
    setSelectedListIDs(tempArrayIDs);
  };

  // Query for all lists available so as to display their names as selectable options.
  // (Then, pass them in as props to render in ListOptions.)
  const { loading, error, data } = useQuery<ListNamesQuery>(LISTNAMES_QUERY);
  const allListNames: Array<string | undefined> = [];
  const allListDescriptions: Array<string | undefined> = [];
  const allListIDs: Array<number | undefined> = [];
  data?.lists?.forEach((list, i) => {
    allListNames.push(list?.name);
    allListDescriptions.push(list?.description);
    // To-do: Refactor TypeScript to store data in array of objects, instead of array of strings. (ID is a number.)
    // ID is pattern-matched from index.
    allListIDs.push(i + 1);
  });
  return (
    <ListJobsContainer>
      <h1>Welcome to Trisha's Favorite Job Lists</h1>
      <RenderListOptions
        lists={allListNames}
        descriptions={allListDescriptions}
        updateSelectedLists={updateSelectedLists}
      />
      <JobsDisplayContainer>
        <DisplayJobs listIDs={selectedListIDs} />
      </JobsDisplayContainer>
    </ListJobsContainer>
  );
};

const ListJobsContainer = styled.div`
  border: 1px solid purple;
  padding: 20px;
  text-align: center;
`;

const JobsDisplayContainer = styled.div`
  border: 1px solid blue;
  padding: 20px;
`;

export default ListJobs;
