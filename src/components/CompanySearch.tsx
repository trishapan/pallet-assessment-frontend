import React, { useState } from "react";
import gql from "graphql-tag";
import { useQuery } from "@apollo/react-hooks";
import { CompanySearchQuery } from "./__generated__/CompanySearchQuery";
import styled from "styled-components";

const COMPANYSEARCH_QUERY = gql`
  query CompanySearchQuery {
    companies(name: "") {
      name
      image
    }
  }
`;

const CompanySearchResult = ({ searchTerm }: { searchTerm: String }) => {
  const { loading, error, data } = useQuery<CompanySearchQuery>(
    COMPANYSEARCH_QUERY,
    { variables: { searchTerm } }
  );

  return (
    <>
      {loading && <h1>Loading</h1>}
      {error && <h1>Error</h1>}
      {data && (
        <h1>
          {data.companies?.map((company, i) => (
            <div key={i}>{company?.name}</div>
          ))}
        </h1>
      )}
    </>
  );
};

const CompanySearch = () => {
  return (
    <CompanySearchContainer>
      <CompanySearchResult searchTerm="enter search term here" />
    </CompanySearchContainer>
  );
};

const CompanySearchContainer = styled.div`
  border: 1px solid blue;
  padding: 20px;
`;

export default CompanySearch;