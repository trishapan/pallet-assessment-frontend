This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

After cloning, run `yarn install` in the directory, and then `yarn schema` to generate types for the Apollo Client fort GraphQL

## Coding Assessment

For this code assessment, we want to see you build a simple web app that integrates React, GraphQL and Django.

Provided is a basic project bootstrapped with Create React App and linked to the provided Django through Graphene/Apollo GraphQL.

### Overview

- We'll provide you with 8 jobs and 4 lists to add to the database, which you'll have to set up yourself.
- Each job will be associated with 1-3 lists. Your end goal should be a product that shows you a feed of the jobs in the list.
- Jobs can be found at the following airtable base: https://airtable.com/shrlh7aFQ5abFTuZW
- Lists can be found at the following airtable base: https://airtable.com/shrmha3fJHJLIk2l5

### Specs and Requirements

- All of the lists (and any information you choose to include about it) should be shown and selectable.
- When a user selects a list, only the jobs in that list should appear. Users should be able to select multiple lists at a time.
- If a user unselects a list, the jobs that are associated with that list should dissappear. However, if a job has multiple lists associated with it, and one list is hidden, the job should still be shown.
- Please include fixtures (https://docs.djangoproject.com/en/3.2/howto/initial-data/) to allow for easy setup of the grader.

### Tips

- Utilise hooks and state where suitable.
- All communication between the React frontend and Django backend should occur through Graphene/Apollo GraphQL.
- The front end should be written using Typescript.
- You're 100% allowed to use other libraries! We recommend [styled components](https://styled-components.com/) for styling
- Use django admin to populate your database (after you've defined it).

### Generating Types From Apollo for GraphQL: `yarn schema`

Running this will sync the Apollo client's types with the GraphQL endpoint (Django Backend)

### Running the app: `yarn start`

In the project directory, you can run:

Runs the app in the development mode.

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.

You will also see any lint errors in the console.

## Learn More (Frontend)

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

## Learn More (Backend)

- https://www.howtographql.com/graphql-python/0-introduction/
- https://docs.graphene-python.org/en/latest/quickstart/
- https://www.apollographql.com/docs/react/data/queries/
- https://docs.djangoproject.com/en/3.2/

## URLs:

- Frontend: http://localhost:3000/
- Backend: http://localhost:8000/
- Backend data visualizer: http://localhost:8000/admin/ (Create a [superuser account](https://docs.graphene-python.org/projects/django/en/latest/tutorial-plain/#load-some-test-data) in order to access)
- Backend GraphiQL query API: http://localhost:8000/graphql/ (See backend > assessment/jobs/migrations/schema.py and scroll to the bottom for sample graphiQL queries)
